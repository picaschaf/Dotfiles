# Straight forward exports
export ZSH=~/.oh-my-zsh
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export LANG=de_DE.UTF-8

export EDITOR="emacsclient -c"
export VISUAL="emacsclient -c"
export ALTERNATE_EDITOR=""

export PATH=~/Qt/5.14.0/clang_64/bin:$PATH
export PATH=~/Qt/5.14.0/gcc_64/bin:$PATH
export PATH=/usr/local/texlive/2018/bin/x86_64-darwin:$PATH

alias e="emacsclient -c"
alias w="cd ~/Workspace"
