#!/bin/sh
set -e # Immediately stop on any error.

# TODO: Think about using brew zsh instead of the outdated
#       macOS shipped zsh.

installBrewPackage() {
    if brew ls --versions $1 >/dev/null; then
        echo "  * $1 is already installed."
    else
        echo "  * Installing $1..."
        brew install $1 &>/dev/null
    fi
}

# Request sudo password if needed as it's output will
# not be displayed anymore.
sudo true


# Install the needed packages for CentOS and Fedora systems.
# Check for Fedora first as it has also redhat-release.
LINUX_PACKAGES="\
    emacs \
    tmux \
    zsh \
    cmake \
    python \
    patch \
    doxygen \
    graphviz \
    clang \
    minicom \
    ripgrep \
"
REDHAT_PACKAGES="\
    gcc-c++ \
    the_silver_searcher \
    util-linux-user \
    awscli \
    fd-find \
"
DEBIAN_PACKAGES="\
    g++ \
    silversearcher-ag \
    fonts-hack-ttf \
    awscli \
"

ARCH_PACKAGES="\
    the_silver_searcher \
    aws-cli \
"

if [ -e "/etc/fedora-release" ]; then
    echo "* Installing Fedora packages if necessary..."
    sudo dnf -y install $LINUX_PACKAGES $REDHAT_PACKAGES &>/dev/null
elif [ -e "/etc/redhat-release" ]; then
    echo "* Installing RedHat packaged if necessary..."
    sudo yum -y install $LINUX_PACKAGES $REDHAT_PACKAGES &>/dev/null
elif [ -e "/etc/debian_version" ]; then
    echo "* Installing Debian/Ubuntu packaged if necessary..."
    sudo apt-get --assume-yes install $LINUX_PACKAGES $DEBIAN_PACKAGES &>/dev/null
elif [ -e "/etc/arch-release" ]; then
    echo "* Installing Arch Linux packaged if necessary..."
    sudo pacman -S --noconfirm $LINUX_PACKAGES $ARCH_PACKAGES
elif [ `uname` = "Darwin" ]; then
    # Install homebrew if not already available.
    if ! [ -x "$(command -v brew)" ]; then
        echo "* Installing homebrew..."
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    else
	    echo "* Homebrew is already installed."
    fi

    # Install packages if they are not installed already.
    installBrewPackage ag
    installBrewPackage cmake
    installBrewPackage doxygen
    installBrewPackage graphviz
    installBrewPackage tmux
    installBrewPackage awscli
    installBrewPackage clang-format
    installBrewPackage libvterm
    installBrewPackage ripgrep

    brew tap d12frosted/emacs-plus
    brew install emacs-plus --with-EmacsIcon1-icon
    ln -s /usr/local/opt/emacs-plus/Emacs.app /Applications/Emacs.app
else
    echo "* This OS is currently not supported by this script."
    exit -1
fi

# Install doom.
if [ ! -d "$HOME/.emacs.d" ]; then
    echo "* Installing doom..."
    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
    ~/.emacs.d/bin/doom install
else
    echo "* Doom is already installed."
fi

# Install oh-my-zsh if it is not already setup.
if [ ! -d "$HOME/.oh-my-zsh" ]; then
    echo "* Installing oh-my-zsh..."
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    zsh -c ls &>/dev/null # Start zsh once to ensure everything is initialized.
else
    echo "* oh-my-zsh is already installed."
fi

# Run the clang_complete refresh script
# TODO: Determine the most recent installed Qt version if available.
~/.workspace-config/refresh_clang-complete.sh 5.14.0

# Create the symlinks to the config files.
echo "* Setting symlinks to config files..."
rm -f ~/.zshrc ~/.zshenv ~/.tmux.conf ~/.spacemacs ~/.gitignore_global ~/.gitconfig
ln -s $PWD/.zshrc ~/.zshrc
ln -s $PWD/.zshenv ~/.zshenv
ln -s $PWD/.gitignore_global ~/.gitignore_global
ln -s $PWD/.gitconfig ~/.gitconfig
ln -s $PWD/.clang_complete ~/.clang_complete
ln -s $PWD/.doom.d ~/.doom.d

# Change default shell
if [ `getent passwd $(whoami) | cut -d: -f7 | rev | cut -d/ -f1 | rev` = "zsh" ]; then
    echo "* zsh is already the login shell."
else
    echo "* Making zsh the login shell..."
    chsh -s $(which zsh) $(whoami)
fi

# Do some OS specific setup...
if [ `uname` = "Darwin" ]; then
    # Credits for most of the macOS specific settings to:
    # https://github.com/nicksp/dotfiles/blob/master/osx/set-defaults.sh

    # Disable inline attachments in Mail.app (just show the icons)
    defaults write com.apple.mail DisableInlineAttachmentViewing -bool true

    # Show all processes in Activity Monitor
    defaults write com.apple.ActivityMonitor ShowCategory -int 0

    # Require password immediately after sleep or screen saver.
    defaults write com.apple.screensaver askForPassword -int 1
    defaults write com.apple.screensaver askForPasswordDelay -int 0

    # Always open everything in Finder's column view. This is important.
    defaults write com.apple.Finder FXPreferredViewStyle Nlsv
elif [ `uname` = "Linux" ]; then
    # Install emacs as systemd service for the current user.
    #echo "* Removing any previous emacs.service ..."
    #sudo systemctl stop emacs@$(whoami).service
    #sudo systemctl disable emacs@$(whoami).service
    #sudo rm /etc/systemd/system/emacs@.service

    #echo "* Installing emacs.service for the current user..."
    #sudo cp emacs@.service /etc/systemd/system/emacs@.service
    #sudo systemctl enable emacs@$(whoami).service
    #sudo systemctl start emacs@$(whoami).service
fi

echo "Done."
