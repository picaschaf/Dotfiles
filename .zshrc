RPROMPT='$(date "+%Y-%m-%d %H:%M:%S %Z")'
ZSH_THEME="../../.workspace-config/my-theme"

plugins=(git brew colored-man-pages colorize cp man osx svn tmux)

source $ZSH/oh-my-zsh.sh

# Add some secret data.
source ~/.private_environment.sh &>/dev/null
