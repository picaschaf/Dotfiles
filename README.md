# workspace-config
This is my workspace configuration.


Installation
---
Directly install via curl:
```
sh -c "`curl -fsSL https://raw.githubusercontent.com/picaschaf/workspace-config
/master/remote-setup.sh`"
```
This will remove any ~/.workspace-config that may is present and do a 
fresh clone and setup of the config.


Supported systems
---
Currently Fedora, CentOS and macOS are tested and working. If any other OS is
used the script will failsafe exit.


Installation procedure
---
1. Install packages (if not already installed) via dnf/yum or homebrew 
   (will be installed if not available on macOS).
* emacs
* tmux
* zsh
* ag
* gcc/g++
* cmake
* python
* patch
* awscli
* doxygen
* graphviz
* util-linux-user
* clang (clang-format on macOS)

2. Install spacemacs distribution if not already installed.

3. Install oh-my-zsh if not already installed.

4. Create symlinks for all the config files
* .zshrc
* .zshenv
* .tmux.conf
* .spacemacs

5. Set the login shell for the current user to zsh if no 
   already done.


Environment
---

Emacs is set as the default editor for the system and aliases for graphical 
Emacs (`e`) and terminal Emacs (`et`) are set.

`w` alias is defined to cd into my default workspace directory.
