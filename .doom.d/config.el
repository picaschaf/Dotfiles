;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Alexander Nassian"
      user-mail-address "nassian@bitshift-dynamics.com")

;; doomb exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(cond
 ((string-equal system-type "darwin")
  (progn
    (setq doom-font (font-spec :family "Fira Code" :size 14))))
 ((string-equal system-type "gnu/linux")
  (progn
    (setq doom-font (font-spec :family "Fira Code" :size 20))))
 )

;; Disable tool and menu bar
(tool-bar-mode -1)
(menu-bar-mode -1)

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

(setq ns-alternate-modifier 'none)

(use-package! window-numbering
  :config
  (map!
   (:leader
    (:desc "Select window 1" "1" #'select-window-1)
    (:desc "Select window 2" "2" #'select-window-2)
    (:desc "Select window 3" "3" #'select-window-3)
    (:desc "Select window 4" "4" #'select-window-4)
    (:desc "Select window 5" "5" #'select-window-5)
    (:desc "Select window 6" "6" #'select-window-6)
    )
   )
  (window-numbering-mode)
  )

(setq initial-frame-alist '((top . 60) (left . 30) (width . 200) (height . 60)))

(setq +doom-dashboard-banner-file (expand-file-name "tesla.png" doom-private-dir))

(map!
 (:leader
  (:desc "M-x" "SPC" #'counsel-M-x)))

(use-package! elfeed
  :config
  (setq elfeed-feeds
        '(("http://nullprogram.com/feed/" blog emacs)
          "http://www.50ply.com/atom.xml"  ; no autotagging
          ("http://nedroid.com/feed/" webcomic)))
)

;; Inhibit that emacs indents the previous line after pressing Enter
(setq-default electric-indent-inhibit t)

;; Get rid of that weird behavior that TAB in the middle of the line inserts \t
(define-key input-decode-map [(control ?i)] [control-i])
(define-key input-decode-map [(control ?I)] [(shift control-i)])
(map! :map 'evil-motion-state-map "C-i" nil)
(define-key evil-motion-state-map [control-i] 'evil-jump-forward)
(setq-default tab-always-indent t)

;; Screw all backup and lock files
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)

;; Let GPG ask for the passphrase if it isn't cached anymore.
(setq epa-pinentry-mode 'loopback)

;; Indentation
(setq-default c-basic-offset 4)
(setq-default c-default-style "linux")
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

;; Color all chars after col 80 red
(setq whitespace-style '(face lines-tail))
(custom-set-faces
 '(whitespace-space ((t (:bold t :foreground "gray75"))))
 '(whitespace-empty ((t (:foreground "firebrick" :background "SlateGray1"))))
 '(whitespace-hspace ((t (:foreground "lightgray" :background "LemonChiffon3"))))
 '(whitespace-indentation ((t (:foreground "firebrick" :background "beige"))))
 '(whitespace-line ((t (:foreground "black" :background "red"))))
 '(whitespace-newline ((t (:foreground "orange" :background "blue"))))
 '(whitespace-space-after-tab ((t (:foreground "black" :background "green"))))
 '(whitespace-space-before-tab ((t (:foreground "black" :background "DarkOrange"))))
 '(whitespace-tab ((t (:foreground "blue" :background "white"))))
 '(whitespace-trailing ((t (:foreground "red" :background "yellow"))))
 )
(add-hook 'c++-mode-hook 'whitespace-mode)
(add-hook 'c-mode-hook 'whitespace-mode)

;; Remove trailing whitespaces for C/C++ files
(add-hook 'c++-mode-hook
          (lambda () (add-to-list 'write-file-functions
                                  'delete-trailing-whitespace)))

;; Add Qt macros without semicolon so they don't break indentation
(defun qt-macros-update ()
  (setq c-macro-names-with-semicolon '("Q_OBJECT"
                                       "Q_DISABLE_COPY"
                                       "Q_DECLARE_PRIVATE"
                                       "Q_DECLARE_PUBLIC"
                                       "Q_PROPERTY"
                                       "Q_DECLARE"
                                       "Q_ENUMS"
                                       "Q_INTERFACES"
                                       "C_NAMESPACE"
                                       "C_ATTR"))
  (c-make-macro-with-semi-re))
(add-hook 'c-mode-common-hook 'qt-macros-update)

;; org-mode
(setq org-directory "~/org")
(setq org-default-notes-file (concat org-directory "/inbox.org"))
(setq org-agenda-files (list "~/org"))
(setq org-refile-targets '((org-agenda-files :maxlevel . 3)))

(defun my-org-skip-subtree-if-priority (priority)
    "Skip an agenda subtree if it has a priority of PRIORITY.

PRIORITY may one of the characters ?A, ?B or ?C."
    (let ((subtree-end (save-excursion (org-end-of-subtree t)))
          (pri-value (* 1000 (- org-lowest-priority priority)))
          (pri-current (org-get-priority (thing-at-point 'line t))))
      (if (= pri-value pri-current)
          subtree-end
        nil)))
  (defun my-org-skip-subtree-if-habit ()
    "Skip an agenda entry if it has a STYLE property equal to \"habit\"."
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (if (string= (org-entry-get nil "STYLE") "habit")
          subtree-end
        nil)))

(setq org-agenda-custom-commands
        '(("c" "My agenda overview"
           ((tags "PRIORITY=\"A\""
                  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "High priority unfinished tasks:")))
            (agenda "")
            (alltodo ""
                     ((org-agenda-skip-function
                       '(or (my-org-skip-subtree-if-priority ?A)
                            (my-org-skip-subtree-if-habit)
                            (org-agenda-skip-if nil '(scheduled deadline))))
                      (org-agenda-overriding-header "ALL normal priority tasks:"))))
           ((org-agenda-compact-blocks t)))))

(setq org-todo-keywords
        (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

(setq org-todo-keyword-faces
        (quote (("TODO" :foreground "red" :weight bold)
                ("NEXT" :foreground "blue" :weight bold)
                ("DONE" :foreground "forest green" :weight bold)
                ("WAITING" :foreground "orange" :weight bold)
                ("HOLD" :foreground "magenta" :weight bold)
                ("CANCELLED" :foreground "forest green" :weight bold)
                ("MEETING" :foreground "forest green" :weight bold)
                ("PHONE" :foreground "forest green" :weight bold))))

;; Paperless
;; * Paperless
;; ** Set directories
;; #+BEGIN_SRC emacs-lisp
;; (custom-set-variables
;; '(paperless-capture-directory "/Volumes/GoogleDrive/Meine Ablage/bitshift dynamics GmbH/Scannen")
;; '(paperless-root-directory "/Volumes/GoogleDrive/Meine Ablage/bitshift dynamics GmbH"))
;; #+END_SRC

;; ** Remap keyboard shortcuts
;; #+BEGIN_SRC emacs-lisp
;; (spacemacs/set-leader-keys
;;   "dp" 'paperless
;;   "dr" 'paperless-rename
;;   "do" 'paperless-display
;;   "df" 'paperless-file
;;   "dd" 'paperless-delete
;;   "dx" 'paperless-execute
;;   "d-" 'paperless-doc-view-shrink
;;   "d+" 'paperless-doc-view-enlarge
;;   "d=" 'paperless-doc-view-scale-reset)
;; #+END_SRC

;; Slack
(setq slack-buffer-create-on-notify t)
(setq slack-prefer-current-team t)
(setq slack-buffer-function #'switch-to-buffer)
(load "~/.slack.el.gpg")
