#!/usr/bin/env bash

find ~/Qt/$1/Src/* -maxdepth 0 -name "qt[a-zA-Z]*" -execdir echo -I~/Qt/$1/Src/{}/include \; > ~/.workspace-config/.clang_complete
echo "-std=c++17" >> ~/.workspace-config/.clang_complete
