#!/bin/sh

rm -fr ~/.workspace-config
cd ~
git clone https://github.com/picaschaf/workspace-config.git .workspace-config
cd .workspace-config
./setup-workspace.sh
